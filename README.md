# wicca-php

This is a vendor package managing system running cross-platform on php.

## Installation: Let's do magic

[Download wicca.phar](https://bitbucket.org/rmp/wicca-php/downloads) into some folder of your project by hand or via
console:

### Linux
```bash
 $ curl -L -s http://bitbucket.org/rmp/wicca-php/downloads/install.php | php
```

## Usage: Mix the philtre in your cauldron

Everything you need is a `cauldron.json` which contains where your repos come from and where they go to.
Read the wiki how to create this file: [Create cauldron.json](https://bitbucket.org/rmp/wicca-php/wiki/cauldron)

## Run: Serve the soup

After you setup the `cauldron.json` file let the `wicca.phar` perform some magic:
```bash
$ php wicca.phar
```

Run it in the same folder as your cauldron is and it will do the work for you.

## Supported ingredients

Currently Wicca can handle with

 - GIT Repositories
 - ZIP Archives

And is tested on

 - Linux (Ubuntu)
 - Windows 7