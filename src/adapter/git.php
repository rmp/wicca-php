<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in GIT.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    GIT.php
 * @author     Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 *
 * @see        NetOther, Net_Sample::Net_Sample()
 */

namespace adapter;

/**
 * Short description for class
 *
 * Long description for class (if any)...
 *
 * @todo       correct class header in GIT.php
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Mike Pretzlaw <pretzaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 */
class git implements adapterInterface
{

    protected $_source = '';

    public function __construct ( $source, $target = null )
    {
        $this->_source = $source;
        $this->_target = $target;
		
		if (!is_dir($this->_target))
		{
			mkdir($this->_target, 0755, true) or die("Could not create target dir");
		}
    }

    public function extract ()
    {
		\console::log("Extract " . $this->_source);
        \helper\os::exec('git clone ' . $this->_source . ' ' . $this->_target);
    }

    public function base ( $baseName )
    {
        $cd = getcwd();
        chdir($this->_target) or die("Could not go to target dir");
        \helper\os::exec('git checkout ' . $baseName);
        chdir($cd);
    }
}
