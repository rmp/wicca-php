<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in GIT.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    GIT.php
 * @author     Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 *
 * @see        NetOther, Net_Sample::Net_Sample()
 */

namespace parser;

/**
 * Short description for class
 *
 * Long description for class (if any)...
 *
 * @todo       correct class header in GIT.php
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Mike Pretzlaw <pretzaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 */
abstract class parserAbstract
{

    const VENDOR         = "vendor";
    const VENDOR_ADAPTER = "adapter";
    const VENDOR_BASE    = "base";
    const VENDOR_SOURCE  = "source";
    const VENDOR_TARGET  = "target";
    const MAPPING        = "mapping";
    const MAPPING_FOLDER = "folder";
}
