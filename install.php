<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in install.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE:
 *
 * @author     Ralf Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Ralf Mike Pretzlaw
 * @license
 * @link       http://www.mike-pretzlaw.de
 *
 * @since
 *
 * @see
 */

/*
 * Place includes, constant defines and $_GLOBAL settings here.
 * Make sure they have appropriate docblocks to avoid phpDocumentor
 * construing they are documented by the page-level docblock.
 */

/**
 * Short description for class
 *
 * Long description for class (if any)...
 *
 * @todo       correct class header in install.php
 *
 * @category
 * @package
 *
 * @author     Ralf Mike Pretzlaw <pretzaw@gmail.com>
 * @copyright  2012 Ralf Mike Pretzlaw
 * @license
 * @link       http://www.mike-pretzlaw.de
 *
 * @since
 *
 * @see
 */
class WiccaInstall
{
    protected $_check = false;
    protected $_force = false;
    protected $_help = false;
    protected $_installDir = '.';
    protected $_quiet = false;
    protected $_verbose = false;

    protected $_verboseMode = false;

    function runCheck ()
    {
        $this->_verbose('Checking environment...');

        $showIniInformation = false;
        $iniPath            = php_ini_loaded_file();
        if ( $iniPath )
        {
            $iniInformation = 'PHP is using: ' . $iniPath;
        }
        else
        {
            $iniInformation = 'Currently no php.ini is in use. Please create one.';
        }

        $this->_verbose('   Phar extension...');
        if ( !extension_loaded('Phar') )
        {
            $errors[] = 'This instance of PHP has no Phar extension.';
            $errors[] = 'Please install the Phar extension.';

            $showIniInformation = true;
        }

        $this->_verbose('   PHP Version...');
        if ( version_compare(PHP_VERSION, '5.3.2', '<') )
        {
            $errors[] = 'Your PHP Version is to old (' . PHP_VERSION . ').';
            $errors[] = 'Please install PHP 5.3.2 or later.';
        }

        if ( version_compare(PHP_VERSION, '5.3.4', '<') )
        {
            $warnings[] = 'Your PHP is not very young anymore.';
            $warnings[] = 'Please upgrade to 5.3.4 or later.';
        }

        $this->_verbose('   CLI Caching...');
        if ( ini_get('apc.enable_cli') )
        {
            $warnings[] = "The apc.enable_cli settings buffer console output.";
            $warnings[] = "Please ensure that this line is in your php.ini:";
            $warnings[] = "    apc.enable_cli = Off";

            $showIniInformation = true;
        }

        /**
         * Output
         */
        if ( $showIniInformation )
        { // ini
            $this->_log($iniInformation);
        }

        if ( !empty( $errors ) )
        { // error messages
            $this->_error('Please fix these errors before installing wicca-php:');
            foreach ( $errors as $message )
            {
                $this->_error($message);
            }

            $this->_verbose('Douh!');

            return false;
        }

        if ( !empty( $warnings ) )
        { // warnings
            $this->_warning('Please fix these warning to have a more stable wicca-php:');
            foreach ( $errors as $message )
            {
                $this->_warning($message);
            }
        }

        return true;
    }

    function install ()
    {
        $this->_verbose('Installing...');

        $targetPath = ( is_dir($this->_installDir) ? rtrim($this->_installDir, '/') . '/' : '' ) . 'wicca.phar';
        $targetFile = realpath($this->_installDir) ? realpath($this->_installDir) : getcwd();
        $targetFile .= DIRECTORY_SEPARATOR . 'wicca.phar';

        $this->_verbose('   Look for old installation...');
        if ( is_readable($targetFile) )
        { // file already exists
            if ( $this->_force )
            { // force is on: overwrite it
                $this->_warning('Forced to overwrite old installation.');
            }
            else
            { // otherwise ask what to do
                $line = null;
                $this->_warning('Found old installation of wicca-php. Do you want to replace it? [Y/n] ');

                do
                {
                    $cliHandle = fopen("php://stdin", 'r');
                    $line      = fgets($cliHandle);
                    $line      = strtolower(trim($line));

                    if ( $line == 'n' )
                    { // user doesn't want to overwrite abort
                        $this->_log('ABORTING!');
                        $this->_exit(0);
                    }

                    if ( $line != 'y' && $line !== '' )
                    {
                        $pardon = array(
                            "Pardon?",
                            "What?",
                            "Que?",
                            "Excuse me?",
                            'What does "' . $line . '" mean?',
                            'Meow?',
                            'Please, just use Y to overwrite or N to quit.',
                            'Please, just use Y to overwrite or N to quit.',
                            'Please, just use Y to overwrite or N to quit.',
                        );

                        shuffle($pardon);
                        $this->_log(current($pardon));
                    }
                }
                while ( $line != 'y' && $line !== '' );
            }

            @unlink($targetFile);
        }

        /**
         * Download
         */
        $this->_log('Downloading.');
        $attempts = 5;
        while ( $attempts-- > 0 )
        {
            $this->_verbose('   Attempt #' . 5 - $attempts);
            // prepare download
            $sourceUrl = ( extension_loaded('openssl') ? 'https' : 'http' )
                         . '://bitbucket.org/rmp/wicca-php/downloads/wicca.phar';

            $errorHandler = new \WiccaErrorHandler();
            set_error_handler(array( $errorHandler, 'handleError' ));

            if ( false === copy($sourceUrl, $targetFile, $this->_getStream()) )
            {
                $this->_error('Could not download wicca-php: ' . $errorHandler->message);
            }

            restore_error_handler();
            if ( $errorHandler->message )
            {
                continue;
            }

            try
            {
                $archive = new Phar( $targetFile );
                unset( $archive );
                break;
            }
            catch ( \Exception $e )
            {
                if ( !( $e instanceof \UnexpectedValueException ) && !( $e instanceof \PharException ) )
                {
                    throw $e;
                }

                unlink($file);

                $this->_log('Phar file is borken, trying again.');
            }
        }

        if ( !$attempts )
        {
            $this->_error('The download failed.');
            $this->_error('ABORTING!');
            exit( 1 );
        }

        chmod($targetFile, 0755);

        $this->_log('Congratulations! Wicca-php has been installed.');
        $this->_log('Usage: php ' . $targetPath);
    }

    function process ( $arguments )
    {

        $this->_check   = in_array('--check', $arguments);
        $this->_force   = in_array('--force', $arguments);
        $this->_help    = in_array('--help', $arguments);
        $this->_quiet   = in_array('--quiet', $arguments);
        $this->_verbose = in_array('--verbose', $arguments);

        $this->_verbose('Hello developer :)');

        if ( $this->_help )
        {
            $this->runHelp();
        }

        $check = $this->runCheck();
        if ( $this->_check )
        {
            if ( $check )
            {
                $this->_log('So far so good.');
            }
            $this->_exit(0);
        }

        if ( $check || $this->_force )
        {
            $this->install();
            $this->_exit(0);
        }

        $this->_exit(1);
    }

    protected function _log ( $message, $lineFeed = true )
    {
        if ( $lineFeed )
        {
            print PHP_EOL;
        }

        if ( $this->_quiet )
        {
            return;
        }

        print $message;

        flush();
    }

    protected function _verbose ( $message, $lineFeed = true )
    {
        if ( $this->_verbose )
        {
            $this->_log($message, $lineFeed);
        }
        else
        {
            $this->_log('.', false);
        }
    }

    protected function _warning ( $message, $lineFeed = true )
    {
        if ( !$this->_quiet )
        {
            $this->_log($message, $lineFeed);
        }
    }

    protected function _error ( $message, $lineFeed = true )
    {
        $this->_log($message, $lineFeed);
    }

    protected function _exit ( $flag )
    {
        print PHP_EOL;

        return exit( $flag );
    }

    function runHelp ()
    {
        $this->_verbose('Showing help...');

        echo 'wicca installer\n';
        echo '~~~~~~~~~~~~~~~\n';
        echo '\n';
        echo 'Options';
        echo '    --check       test if wicca-php runs in this environment';
        echo '    --force       force to install wicca-php';
        echo '    --help        this help';
        echo '    --quiet       no output during install';
        echo '    --verbose     show additional info during install';

        $this->_exit(0);
    }

    protected function _getStream ()
    {
        $streamOptions['http'] = array();

        if ( !empty( $_SERVER['HTTP_PROXY'] ) || !empty( $_SERVER['http_proxy'] ) )
        { // proxy given: use it
            $proxy = parse_url(!empty( $_SERVER['http_proxy'] ) ? $_SERVER['http_proxy'] : $_SERVER['HTTP_PROXY']);
        }

        if ( !empty( $proxy ) )
        {
            $proxyURL = isset( $proxy['scheme'] ) ? $proxy['scheme'] . '://' : '';
            $proxyURL .= isset( $proxy['host'] ) ? $proxy['host'] : '';

            if ( isset( $proxy['port'] ) )
            {
                $proxyURL .= ":" . $proxy['port'];
            }
            elseif ( 'https://' == substr($proxyURL, 0, 8) )
            {
                $proxyURL .= ":443";
            }
            elseif ( 'http://' == substr($proxyURL, 0, 7) )
            {
                $proxyURL .= ":80";
            }

            // proxy uses tcp / ssl
            $proxyURL = strtr(array( 'http://' => 'tcp://', 'https://' => 'ssl://' ), $proxyURL);

            if ( 0 === strpos($proxyURL, 'ssl:') && !extension_loaded('openssl') )
            {
                $this->_warning('Please enable the openssl extension for downloading wicca-php.');
            }

            $streamOptions['http'] = array(
                'request_fulluri' => true,
                'proxy'           => $proxyURL,
            );

            if ( isset( $proxy['user'] ) )
            {
                $proxyAuth = $proxy['user'];
                if ( isset( $proxy['pass'] ) )
                {
                    $proxyAuth .= ':' . $proxy['pass'];
                }
                $proxyAuth = base64_encode($proxyAuth);

                $streamOptions['http']['header'] = 'Proxy-Authorization: Basic {$proxyAuth}\r\n';
            }
        }

        return stream_context_create($streamOptions);
    }
}

class WiccaErrorHandler
{
    public $message = '';

    public function handleError ( $code, $msg )
    {
        if ( $this->message )
        {
            $this->message .= '\n';
        }
        $this->message .= preg_replace('{^copy\(.*?\): }', '', $msg);
    }
}

$installer = new WiccaInstall();
$installer->process($argv);

?>
