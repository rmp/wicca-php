<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in Parser.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    Parser.php
 * @author     Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 *
 * @see        NetOther, Net_Sample::Net_Sample()
 */

namespace parser;

/**
 * Short description for class
 *
 * Long description for class (if any)...
 *
 * @todo       correct class header in Parser.php
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Mike Pretzlaw <pretzaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 */
class json extends parserAbstract
{

    /**
     * @var array
     */
    protected $_data = array();

    function __construct ( $json )
    {
        $this->_data = json_decode($json, true);
    }

    function parseSingleVendor ( $key, $config )
    {
        if ( false == isset( $config[self::VENDOR_ADAPTER] ) )
        { // if no adapter set: find possible adapter
            if ( false !== strpos($config[self::VENDOR_SOURCE], '.git') )
            { // if source contains .git: assume git
                $config[self::VENDOR_ADAPTER] = 'git';
            }
            else if ( false !== strpos($config[self::VENDOR_SOURCE], '.zip') )
            {
                $config[self::VENDOR_ADAPTER] = 'zip';
            }
            else
            { // snap, couldn't figure it out
                throw new \ErrorException(
                    'Unknown source. Please provide specific adapter or correct the source of ' . $key
                );
            }
        }

        $adapterClass = "adapter\\" . $config[self::VENDOR_ADAPTER];


        if ( false == isset( $config[self::VENDOR_TARGET] ) )
        {
            $config[self::VENDOR_TARGET] = self::VENDOR . DIRECTORY_SEPARATOR . $key;
        }

        \console::debug('clone ' . $config[self::VENDOR_SOURCE] . ' to ' . $config[self::VENDOR_TARGET]);

        /** @var $adapter \adapter\adapterInterface */
        $adapter = new $adapterClass( $config[self::VENDOR_SOURCE], $config[self::VENDOR_TARGET] );
        $adapter->extract();

        if ( isset( $config[self::VENDOR_BASE] ) )
        {
            $adapter->base($config[self::VENDOR_BASE]);
        }
    }

    function parseMapping ( $mapping )
    {
        if ( isset( $mapping[self::MAPPING_FOLDER] ) )
        {
            $this->parseFolderMapping($mapping[self::MAPPING_FOLDER]);
        }
    }

    function parseFolderMapping ( $mapping )
    {
        foreach ( $mapping as $link => $targetPattern )
        {
            $targetSet = glob($targetPattern);
            foreach ( $targetSet as $targetNode )
            {
                $linkNode   = $link;
                $targetNode = realpath($targetNode);
                if ( is_dir($targetNode) )
                {
                    if ( $targetNode != $targetPattern )
                    { // nodes different to target: a wildcard were used
                        $linkNode = $link . DIRECTORY_SEPARATOR . basename($targetNode);
                    }
                    \helper\os::linkDirectory($linkNode, $targetNode);
                }
            }
        }
    }

    function run ()
    {
        echo "Parser running\n";
        if ( isset( $this->_data[self::VENDOR] ) )
        {
            foreach ( $this->_data[self::VENDOR] as $id => $config )
            {
                $this->parseSingleVendor($id, $config);
            }
        }

        if ( isset( $this->_data[self::MAPPING] ) )
        {
            $this->parseMapping($this->_data[self::MAPPING]);
        }
    }
}
