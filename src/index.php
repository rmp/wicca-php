<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in index.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    index.php
 * @author     Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 *
 * @see        NetOther, Net_Sample::Net_Sample()
 */

require_once 'console.php';

echo "Running wicca-php\n";

/**
 * Enhanced PSR-0 autoloader with Phar-Support
 *
 * @param $class string Name of the class (with namespace)
 *
 * @author    Ralf Mike Pretzlaw <kontakt@mike-pretzlaw.de>
 * @copyright 2012 Ralf Mike Pretzlaw
 *
 * @throws Exception
 */
function __autoload ( $class )
{
    /**
     * PSR-0
     * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
     */
    $className = ltrim($class, '\\');
    $fileName  = '';
    $namespace = '';
    if ( $lastNsPos = strripos($className, '\\') )
    {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    /**
     * PHAR-Part
     */
    $pharFile = Phar::running() . DIRECTORY_SEPARATOR . $fileName;

    if ( false !== stream_resolve_include_path($fileName) )
    { // exists in at least one include-path: require it
        require_once $fileName;
    }
    else if ( file_exists($pharFile) )
    { // is in phar archive: require the phar file
        require_once $pharFile;
    }
    else
    { // neither phar nor in include-path: that's bad
        throw new \Exception( 'File with definition of ' . $class . ' could not be found.' );
    }

    if ( false == class_exists($class, false) && false == interface_exists($class, false) )
    { // class doesn't exists after including a file: bad too
        throw new \Exception( 'Definition for ' . $class . ' not found.' );
    }
}

$contents = file_get_contents("cauldron.json");

if ( false == $contents )
{
    console::console("ERROR: No wicca.json found");
}

$parser = new parser\json( $contents );
$parser->run();