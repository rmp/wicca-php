<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in runTest.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    runTest.php
 * @author     Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 *
 * @see        NetOther, Net_Sample::Net_Sample()
 */

$path    = realpath(getcwd());
$tmpPath = '';

echo "Search wicca-php\n";

while ( $path != $tmpPath && !file_exists($path . DIRECTORY_SEPARATOR . 'src/index.php') )
{
    $tmpPath = $path;
    $path    = realpath($path . DIRECTORY_SEPARATOR . '/..');
}

if ( !file_exists($path . DIRECTORY_SEPARATOR . 'src/index.php') )
{
    echo "No index.php found";
}

$test = str_replace($path, '', __DIR__);
echo "Run wicca-php in $test\n";

include $path . DIRECTORY_SEPARATOR . 'src/index.php';