<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @todo       correct file header in GIT.php
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   CategoryName
 * @package    GIT.php
 * @author     Mike Pretzlaw <pretzlaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 *
 * @see        NetOther, Net_Sample::Net_Sample()
 */

namespace adapter;

/**
 * Short description for class
 *
 * Long description for class (if any)...
 *
 * @todo       correct class header in GIT.php
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Mike Pretzlaw <pretzaw@gmail.com>
 * @copyright  2012 Mike Pretzlaw
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 */
class zip implements adapterInterface
{

    protected $_source = '';

    public function __construct ( $source, $target = null )
    {
        $this->_source = $source;
        $this->_target = $target;

        if ( !is_dir('var/cache') )
        {
            mkdir('var/cache', 0755, true);
        }
    }

    public function extract ()
    {
        $basename = basename(parse_url($this->_source, PHP_URL_PATH));
        $cache    = 'cache/' . $basename;

        if ( file_exists($cache) )
        {
            \console::log('Using cached ' . $basename);
        }
        else
        {
            $this->_download($cache);
        }

        $cd = getcwd();

        if ( !is_dir($this->_target) )
        {
            mkdir($this->_target, 0755, true);
        }

        $zipFile = realpath($cache);

        chdir($this->_target);
        $this->_unzip($zipFile);
        chdir($cd);

    }

    protected function _download ( $cache )
    {
        \console::debug('Get ' . $this->_source);

        mkdir(dirname($cache), 0755, true);

        $downloadHandler = fopen($this->_source, 'r');

        if ( !is_resource($downloadHandler) )
        {
            \console::error('Could not find "' . $this->_source . '"! Is allow_url_open enabled?');

            return;
        }

        $cacheHandler = fopen($cache, 'w');

        if ( !is_resource($cacheHandler) )
        {
            \console::error('Could not write to "' . $cache . '"!');

            return;
        }

        while ( !feof($downloadHandler) )
        {
            fwrite($cacheHandler, fread($downloadHandler, 4096));
        }

        fclose($downloadHandler);
        fclose($cacheHandler);
    }

    public function base ( $baseName )
    {
        $cd = getcwd();
        chdir($this->_target);
        \helper\os::exec('git checkout ' . $baseName);
        chdir($cd);
    }

    protected function _unzip ( $file )
    {
        if ( !function_exists('zip_open') )
        {
            \console::error('Your PHP has no zip extension enabled');
        }

        $zip = zip_open($file);
        if ( is_resource($zip) )
        { // file opened: unzip it
            $tree = "";
            \console::log('Unpacking ' . basename($file));
            while ( ( $zip_entry = zip_read($zip) ) !== false )
            { // iterate over every file
                if ( strpos(zip_entry_name($zip_entry), DIRECTORY_SEPARATOR) !== false )
                { // directory seperator found: handle relative to the previous
                    $last = strrpos(zip_entry_name($zip_entry), DIRECTORY_SEPARATOR);
                    $dir  = substr(zip_entry_name($zip_entry), 0, $last);
                    $file = substr(
                        zip_entry_name($zip_entry),
                        strrpos(zip_entry_name($zip_entry), DIRECTORY_SEPARATOR) + 1
                    );

                    if ( !is_dir($dir) )
                    { // directory doesn't exist: create it
                        @mkdir($dir, 0755, true) or die( "Unable to create $dir\n" );
                    }

                    if ( strlen(trim($file)) > 0 )
                    { // file not empty: create it
                        $return = @file_put_contents(
                            $dir . "/" . $file,
                            zip_entry_read($zip_entry, zip_entry_filesize($zip_entry))
                        );

                        if ( $return === false )
                        { // put contents failed: halt with error
                            die( "Unable to write file $dir/$file\n" );
                        }
                    }
                }
                else
                { // no directory seperator: extract the file in the base dir
                    file_put_contents($file, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
                }
            }
        }
        else
        { // file not opened: show error
            \console::error("Unable to open zip file " . $file);
        }
    }
}
